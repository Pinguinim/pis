#! /bin/bash

###################################################################
#### This repository hosts a collection of BASH scripts        ###
#### I run after a standard (Vanilla) Ubuntu LTS installation  ###
##################################################################

## Pre-installation setup

	# Configure scripts
	chmod 755 ./scripts/*
	chmod 644 ./configs/*
	chown -R root:root $(pwd)/*

	# Backup system scripts
	cp /etc/default/apport /etc/default/apport.orig

    # Install scripts
    mv ./scripts/sysupdate.bash /usr/local/bin/
    mv ./scripts/restart_required.bash /usr/local/bin/
    mv ./scripts/snap_revision_cleanup.bash /usr/local/bin/

## Define Functions
    source ./configs/function_statements

## Install prerequisites
    apt -y install linux-headers-$(uname -r) dselect libxdo3 build-essential debconf-utils libxdo3
    update_system

    # Install additional software
    install_nvidia_drivers
    install_nvme_cli
    install_auto_cpufreq
    enable_flatpak
    install_latest_google_chrome
#    install_rustdesk_client

    # Configure System
    set_favorite_apps
    disable_apport_daemon
    unhide_startup_programs
    disable_welcome_wizard
    accept_msfonts_eula
    install_grub_theme

    # Identify the distro and continues the installation accordingly
    case $(lsb_release -sc) in

        focal)
            # Configure System
            add_ppas
            update_system
            unsnap

            # Install additional software
            dselect update
            dpkg --set-selections < ./configs/focal_pkgs.lst
            apt-get -y dselect-upgrade
            update_system
    ;;

        jammy)
            # Configure System
            update_system
            unsnap

            # Install additional software
            dselect update
            dpkg --set-selections < ./configs/jammy_pkgs.lst
            apt-get -y dselect-upgrade
            update_system
    ;;

        noble)
            # Configure System
            update_system
            unsnap_2404

            # Install additional software
            dselect update
            dpkg --set-selections < ./configs/noble_pkgs.lst
            apt-get -y dselect-upgrade
            update_system
esac

##  Cleanup
    rm -rf ./installers/*
